

//v0   3/1/17 (PM)
import java.util.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RecursiveTask;

import linda.Linda;
import linda.Tuple;

//---------------------------------------------------------------------------------------
public class AlignementSeq {
	
	
    static int similitude(char [] s, char [] t) {
        int [][] tableSimilitude = new int [s.length][t.length];

        //System.out.println("s ("+s.length+") "+s);
        //System.out.println("t ("+t.length+") "+t);
        //initialisation
        tableSimilitude[0][0] = 0;
        //colonne 0
        for (int i=1 ; i<s.length ; i++) {
            tableSimilitude[i][0]=tableSimilitude[i-1][0]+Sequence.suppression(s[i]);
        }
        //ligne 0
        for (int j=1 ; j<t.length ; j++) {
            tableSimilitude[0][j]=tableSimilitude[0][j-1]+Sequence.insertion(t[j]);
        }
        //remplissage ligne par ligne
        for (int i=1 ; i<s.length ; i++) {
            for (int j=1 ; j<t.length ; j++) {
                tableSimilitude[i][j]=Math.max(tableSimilitude[i-1][j]+Sequence.suppression(s[i]),
                                               Math.max(tableSimilitude[i][j-1]+Sequence.insertion(t[j]),
                                                        tableSimilitude[i-1][j-1]+Sequence.correspondance(s[i],t[j])));
            }
        }
        // résultat (minimal : on pourrait aussi donner le chemin et les transformations,
        // mais on se limite à l'essentiel)
        return tableSimilitude[s.length-1][t.length-1];
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        long départ, fin;
        int résultat;

        final Linda linda = new linda.shm.CentralizedLinda();
        BDSequences BDS = new BDSequences();
        BDSequences cible = new BDSequences();

        if (args.length == 2) { //analyse des paramètres
            try {
                BDS.lier(args[0]);
                cible.lier(args[1]);
            }
            catch (IOException iox) {
                throw new IllegalArgumentException("Usage : AlignementSeq <chemin BD> <chemin cible>");
            }
        }
        if (BDS.estVide() || cible.estVide())
                throw new IllegalArgumentException("Usage : AlignementSeq <chemin BD> <chemin cible>");

        //appel correct
        départ = System.nanoTime();
        résultat = AlignementSeq.AMono(BDS,cible,0);
        fin = System.nanoTime();
        System.out.println("test mémoire monoactivité : durée = "+ (fin-départ) /1_000+
        												"µs -> résultat : " + résultat);
                           
        départ = System.nanoTime();
        résultat = AlignementSeq.AMonoLinda(BDS,cible,0,linda);
        fin = System.nanoTime();
        System.out.println("test linda monoactivité : durée = "+ (fin-départ) /1_000+
                										"µs -> résultat : " + résultat);
    }

    static int AMono(BDSequences BD,BDSequences BDcibles,int position) {
        // version "directe", sans Linda, donnée à titre de documentation/spécification
        int score=0;
        int résultat=0;
        Sequence res = null;
        Sequence courant = null;
        Sequence cible = BDcibles.lire(position);
        Iterator<Sequence> it = BD.itérateur();

        while (it.hasNext()) {
            courant = it.next();
            score = similitude(courant.lireSéquence().toCharArray(),cible.lireSéquence().toCharArray());
            if (score > résultat) {
                res = courant;
                résultat = score ;
            }
        }
        
        System.out.println("cible : "+cible.afficher());
        System.out.println("résultat ("+résultat+"/ "+
                           100*résultat/(cible.lireTailleSeq()*Sequence.correspondance('A','A'))+"%): "+res.afficher());
        return résultat;
    }

    static int AMonoLinda(BDSequences BD,BDSequences BDcibles,int position,Linda l) throws InterruptedException, ExecutionException {
        /* Cette version fait transiter les données par l'espace de tuples,
         * ce qui correspond effectivement au contexte d'exécution où l'on se place.
         * Elle est clairement moins efficace que la précédente
         * (passage par Linda, boucles distinctes pour la lecture et le traitement...),
         * mais représente une base pour une parallélisation.
         *
         * Noter que cette version peut/doit être adaptée (simplement), pour permettre de
         * traiter un volume de données supérieur à la mémoire disponible.
         */

        int score=0;
        int résultat=0;
        Sequence res = null;
        Sequence courant = null;
        Sequence cible = BDcibles.lire(position);
        Iterator<Sequence> it = BD.itérateur();
        Tuple tCourant = null;
        
        Tuple tRes = null;

        //déposer la cible dans l'espace de tuples
        l.write(new Tuple("cible",cible.lireSéquence(),cible.afficher(),cible.lireTailleSeq()));

        //déposer les séquences dans l'espace de tuples
        long time = System.currentTimeMillis();
        while (it.hasNext()) {
            courant = it.next();
            l.write(new Tuple("BD",courant.lireSéquence(),courant.afficher()));
        }
        System.out.println("TIME BD : " + (System.currentTimeMillis()-time));

        //chercher la meilleure similitude
        final Tuple tCible = l.take(new Tuple("cible", String.class, String.class, Integer.class));
        
//        tCourant = l.tryTake(new Tuple("BD",String.class,String.class));
//        
//        while (tCourant != null) {
//        	
//    		
//
//            tCourant = l.tryTake(new Tuple("BD",String.class,String.class));
//        }


        
         /** //TEST NUMERO 2 : POOL DE THREADS
        ExecutorService poule = Executors.newFixedThreadPool(4);

        // List of future results
        List<Future<Tuple>> futureTuples = new ArrayList<Future<Tuple>>();
        
        tCourant = l.tryTake(new Tuple("BD",String.class,String.class));
        time = System.currentTimeMillis();
        while (tCourant != null) {
        	
    		Future<Tuple> tuple = poule.submit(new TraiterTuple(tCourant, tCible));
    		futureTuples.add(tuple);

            tCourant = l.tryTake(new Tuple("BD",String.class,String.class));
        }
        System.out.println("TEMPS DE BOUCLE " + (System.currentTimeMillis() - time));
        
        poule.shutdown();

        time = System.currentTimeMillis();
        Tuple tmp = null;
        for (int i=0 ; i<futureTuples.size() ; i++) {
        	tmp = futureTuples.get(i).get();
        	
        	if ((Integer)tmp.get(1) > résultat) {
        		résultat = (Integer)tmp.get(1);
        		tRes = (Tuple)tmp.get(0);
        	}
        }       
        System.out.println("TIME FOR : " + (System.currentTimeMillis() - time));
       **/
        
        /** // TEST NUMERO 1 : CREATION DE THREAD A CHAQUE TUPLE 
        ArrayList<Tuple> tuplesScores = new ArrayList<Tuple>();
        
        tCourant = l.tryTake(new Tuple("BD",String.class,String.class));
        time = System.currentTimeMillis();
        while (tCourant != null) {
        	final Tuple curr = tCourant;
        	new Thread(){
        		public void run() {        			
        			int currScore = similitude(((String)curr.get(1)).toCharArray(),
        	                  ((String)tCible.get(1)).toCharArray());
        			tuplesScores.add(new Tuple(curr,currScore));
        		}
        	}.start();
        	tCourant = l.tryTake(new Tuple("BD",String.class,String.class));
        }
        System.out.println("TEMPS DE BOUCLE " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        for (int i=0 ; i<tuplesScores.size() ; i++) {
        	if (résultat < (Integer)tuplesScores.get(i).get(1)) {
        		résultat = (Integer)tuplesScores.get(i).get(1);
        		tRes = (Tuple)tuplesScores.get(i).get(0);
        	}        	
        }
        System.out.println("TEMPS FOR : " + (System.currentTimeMillis()-time));        
        **/
        
        System.out.println("cible : "+tCible.get(2));
        System.out.println("résultat ("+résultat+"/ "+
                           100*résultat/(((Integer)tCible.get(3))*Sequence.correspondance('A','A'))
                           +"%): "+tRes.get(2));
        return résultat;
    }
    
    


}

class TraiterTuple implements Callable<Tuple>{
	private Tuple tcourant;
	private Tuple tcible;
	
	public TraiterTuple(Tuple tc, Tuple tci) {
		tcourant = tc;
		tcible = tci;
		
	}

	@Override
	public Tuple call() {
		 int score;
	     score = AlignementSeq.similitude(((String)tcourant.get(1)).toCharArray(),
		                  ((String)tcible.get(1)).toCharArray());		     
	     return new Tuple(tcourant, score);
	        	
	}
	
}