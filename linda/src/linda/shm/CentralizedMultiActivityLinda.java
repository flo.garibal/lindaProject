package linda.shm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import linda.Callback;
import linda.Linda;
import linda.Tuple;
import linda.outils.SemaphoreLinda;

/** Shared memory implementation of Linda. */
public class CentralizedMultiActivityLinda implements Linda {

	private List<Tuple> tuples;
	private Condition cond_take;
	private Condition cond_read;
	private Lock lock;
	private boolean lecteur;
	private Map<Tuple,List<CallEvent>> waitingCallEvents;
	private int nbThreads;
	
    public CentralizedMultiActivityLinda(int nbThreads) {
    	tuples = new ArrayList<Tuple>();
    	lock = new ReentrantLock();
    	cond_take = lock.newCondition();
    	cond_read = lock.newCondition();
    	lecteur = false;
    	waitingCallEvents = new HashMap<Tuple,List<CallEvent>>();
    	if (nbThreads <= 0)
    		this.nbThreads = 1;
    	else
    		this.nbThreads = nbThreads;
    }

	@Override
	public void write(Tuple t) {
		lock.lock();
		
		Tuple tmpTuple = t.deepclone();
		tuples.add(tmpTuple);
		
		lecteur = false;

		cond_read.signalAll();
		
		if (!lecteur) {
			cond_take.signal();
		}
		wakeUpCallBack(t);
		lock.unlock();
	}

	@Override
	public Tuple take(Tuple template) {
		Tuple returnedTuple = null;
		int i = 0; int fin=0;
		SemaphoreLinda sem = new SemaphoreLinda("Sem", 1);
		lock.lock();
		
		while (returnedTuple == null) {
			while (i<fin && returnedTuple == null) {
				if (tuples.get(i).matches(template)) {
					sem.acquire();
					returnedTuple = tuples.get(i);
					sem.release();
					tuples.remove(i);
				}
				i++;
			}
			if (returnedTuple == null) {
				try {
					cond_take.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			i = 0;
		}
		
		lock.unlock();
		
		return returnedTuple;
	}

	@Override
	public Tuple read(Tuple template) {
		lecteur = true;
		Tuple returnedTuple = null;
		int i = 0;
				
		lock.lock();
		
		while (returnedTuple == null) {
			while (i<tuples.size() && returnedTuple == null) {
				if (tuples.get(i).matches(template)) {
					returnedTuple = tuples.get(i).deepclone();
				}
				i++;
			}
			i = 0;
			if (returnedTuple == null) {
				try {
					cond_read.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		lock.unlock();
		
		return returnedTuple;
	}

	@Override
	public Tuple tryTake(Tuple template) {
		Tuple returnedTuple = null;
		int i = 0;
				
		while (i<tuples.size() && returnedTuple == null) {
			if (tuples.get(i).matches(template)) {
				returnedTuple = tuples.get(i);
				tuples.remove(i);
			}
			i++;
		}
		
		return returnedTuple;
	}

	@Override
	public Tuple tryRead(Tuple template) {
		Tuple returnedTuple = null;
		int i = 0;
		
		while (i<tuples.size() && returnedTuple == null) {
			if (tuples.get(i).matches(template)) {
				returnedTuple = tuples.get(i).deepclone();
			}
			i++;
		}
		
		return returnedTuple;
	}

	@Override
	public Collection<Tuple> takeAll(Tuple template) {
		List<Tuple> tuplesReturned = new ArrayList<Tuple>();
		
		lock.lock();
		
		for (Tuple t: tuples) {
			if (t.matches(template)) {
				tuplesReturned.add(t);
			}
		}
		
		lock.unlock();
		tuples.removeAll(tuplesReturned);
		
		return tuplesReturned;
	}

	@Override
	public Collection<Tuple> readAll(Tuple template) {
		List<Tuple> tuplesReturned = new ArrayList<Tuple>();
		
		for (Tuple t: tuples) {
			if (t.matches(template)) {
				tuplesReturned.add(t);
			}
		}
				
		return tuplesReturned;
	}

	@Override
	public void eventRegister(eventMode mode, eventTiming timing, Tuple template, Callback callback) {
		Tuple tmp ;
		if(timing == eventTiming.IMMEDIATE) {
			
			if(mode == eventMode.TAKE) {
				tmp = tryTake(template);
				
			} else {
				tmp = tryRead(template);
			}
			
			if (tmp != null) {
				callback.call(tmp);
			} else {
				saveCallEvent(template, mode, callback);
			}
			
		} else{
			saveCallEvent(template, mode, callback);
		}
	}

	@Override
	public void debug(String prefix) {
		System.out.println();
		System.out.println();
		System.out.print(prefix + " Tuples : [");
		for(Tuple t : tuples){
			System.out.print(t);
		}
		System.out.println("]");
		
		System.out.print(prefix + " CallEvents : [");
		for(Entry<Tuple, List<CallEvent>> entry : waitingCallEvents.entrySet()){
			System.out.print("{" + entry.getKey() + " -> " + entry.getValue().size() + "}");
		}
		System.out.println("]");		
		System.out.println();
		System.out.println();
	}
	
	/** Met un callback en attente sur le bon motif de tuple qu'il recherche
	 * @param Callback le callback en question
	 * @param eventMode l'eventMode du callback
	 * @param Tuple le motif du tuple
	 **/
	private void saveCallEvent(Tuple template, eventMode mode, Callback callback) {
		boolean found = false;
		CallEvent currCallEvent = new CallEvent(mode, template, callback);
		List<CallEvent> currWaitingCallEvents = null;
		
		
		Iterator<Entry<Tuple, List<CallEvent>>> it = waitingCallEvents.entrySet().iterator();
	    while (it.hasNext() && !found) {
	        Entry<Tuple,List<CallEvent>> pair = (Entry<Tuple, List<CallEvent>>) it.next();
	         
	        /* Si le template correspond à l'ID de la map */
	        if(template.matches(pair.getKey())) {
	        	found = true;
	        	currWaitingCallEvents = pair.getValue();
	        }
	    }
		if(currWaitingCallEvents != null) {
			currWaitingCallEvents.add(currCallEvent);
			
		} else {
			List<CallEvent> tmpList = new ArrayList<CallEvent>();
			
			tmpList.add(currCallEvent);
			
			waitingCallEvents.put(template,tmpList);
		}
	}
	
	/** A partir d'un motif, cherche le callEvent à appeler et l'appelle. 
	 * Si plusieurs callEvent sont présents pour le même motif, tant que l'eventMode
	 * des callback n'est pas eventMode.TAKE, on les appelle tous.
	 * @param Tuple
	 **/
	private void wakeUpCallBack(Tuple template) {
		
		boolean found = false;
		boolean take = false;
		List<CallEvent> currWaitingCallEvents = null;
		
		/* RECUPERATION DES CALL EVENT EN ATTENTE SI EXISTANT */
		
		Iterator<Entry<Tuple, List<CallEvent>>> it = waitingCallEvents.entrySet().iterator();
	    while (it.hasNext() && !found) {
	        Entry<Tuple,List<CallEvent>> pair = (Entry<Tuple, List<CallEvent>>) it.next();
	         
	        /* Si le template correspond à l'ID de la map */
	        if(template.matches(pair.getKey())) {
	        	found = true;
	        	currWaitingCallEvents = pair.getValue();
	        }
	    }


		/* SI CALL EVENTS EN ATTENTE ALORS ON REVEILLE TOUS LES READ JUSQU'AU PREMIER "TAKE" */
		if (currWaitingCallEvents != null) {
			int i = 0;
			while (i < currWaitingCallEvents.size() && !take) {
				CallEvent currCallEvent = currWaitingCallEvents.get(i);
				if (currCallEvent.getMode() == eventMode.READ) {

					currCallEvent.getCallback().call(template);

				} else {
					take = true;
					currCallEvent.getCallback().call(template);
					tuples.remove(template);
				}
				i++;
			}
		}
	}
}
