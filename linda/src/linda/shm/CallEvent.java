package linda.shm;

import linda.Callback;
import linda.Linda.eventMode;
import linda.Tuple;

public class CallEvent {


	private eventMode mode;
	private Callback callback;
	
	public CallEvent (eventMode m, Tuple templ, Callback c) {
		mode = m;
		callback = c;
	}

	public eventMode getMode() {
		return mode;
	}

	public Callback getCallback() {
		return callback;
	}	
}
