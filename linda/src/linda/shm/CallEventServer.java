package linda.shm;

import java.io.Serializable;

import linda.Linda.eventMode;
import linda.SerializableCallback;
import linda.Tuple;

public class CallEventServer implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1620334735080123810L;
	private eventMode mode;
	private String callback;
	
	public CallEventServer (eventMode m, Tuple templ, String callback2) {
		mode = m;
		callback = callback2;
	}

	public eventMode getMode() {
		return mode;
	}

	public String getCallback() {
		return callback;
	}	
}
