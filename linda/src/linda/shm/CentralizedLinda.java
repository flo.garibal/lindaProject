package linda.shm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import linda.Callback;
import linda.Linda;
import linda.Tuple;

/** Shared memory implementation of Linda. */
public class CentralizedLinda implements Linda {

	private Integer tailleMax ; //taille du tuple
	private List<Tuple> tuples;
	private Condition cond_take;
	private Condition cond_read;
	private Lock lock;
	private boolean lecteur;
	private Map<Tuple,List<CallEvent>> waitingCallEvents;
	
    public CentralizedLinda(int taille) {
    	tuples = new ArrayList<Tuple>();
    	lock = new ReentrantLock();
    	cond_take = lock.newCondition();
    	cond_read = lock.newCondition();
    	lecteur = false;
    	waitingCallEvents = new HashMap<Tuple,List<CallEvent>>();
    	tailleMax = taille;
    }
    public CentralizedLinda(){
    	this(50000);
    	
    }

    /**
     * Return the max size of the pool
     * @return max pool size
     */
	public Integer getTailleMax() {
		return tailleMax;
	}
	
	/**
	 * Set the size max of the pool
	 * @param tailleMax
	 */
	public void setTailleMax(Integer tailleMax) {
		this.tailleMax = tailleMax;
	}
	
	/**
	 * Return the current size of the pool
	 * @return pool size
	 */
	public Integer getTailleCourante() {
		return tuples.size();
	}
    
	
	/**
	 * Return the size of the waiting call events according to the tuple pattern given
	 * @param t : the pattern
	 * @return size of the list, 0 if it doesn't exist
	 */
	public Integer getTailleFileAttente(Tuple t) {
		Integer size = 0;
		if (waitingCallEvents.get(t) != null) {
			size = waitingCallEvents.get(t).size();
		} 
		return size; 
	}
	
	@Override
	public void write(Tuple t) {
		if (tuples.size()+1  >= tailleMax) {
			System.out.println("Ajout impossible: Taille de l'espace trop petit");
		}
		else {
			lock.lock();
		
			Tuple tmpTuple = t.deepclone();
			tuples.add(tmpTuple);
		
			lecteur = false;

			cond_read.signalAll();
		
			if (!lecteur) {
				cond_take.signal();
			}
			wakeUpCallBack(t);
			lock.unlock();
		}
	}

	@Override
	public Tuple take(Tuple template) {
		Tuple returnedTuple = null;
		int i = 0;
		
		lock.lock();
		
		while (returnedTuple == null) {

			while (i<tuples.size() && returnedTuple == null) {
				if (tuples.get(i).matches(template)) {
					returnedTuple = tuples.get(i);
					tuples.remove(i);
				}
				i++;
			}
			if (returnedTuple == null) {
				try {
					cond_take.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			i = 0;
		}
		
		lock.unlock();
		
		return returnedTuple;
	}

	@Override
	public Tuple read(Tuple template) {
		lecteur = true;
		Tuple returnedTuple = null;
		int i = 0;
				
		lock.lock();
		
		while (returnedTuple == null) {
			while (i<tuples.size() && returnedTuple == null) {
				if (tuples.get(i).matches(template)) {
					returnedTuple = tuples.get(i).deepclone();
				}
				i++;
			}
			i = 0;
			if (returnedTuple == null) {
				try {
					cond_read.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		lock.unlock();
		
		return returnedTuple;
	}

	@Override
	public Tuple tryTake(Tuple template) {
		Tuple returnedTuple = null;
		int i = 0;
				
		while (i<tuples.size() && returnedTuple == null) {
			if (tuples.get(i).matches(template)) {
				returnedTuple = tuples.get(i);
				tuples.remove(i);
			}
			i++;
		}
		
		return returnedTuple;
	}

	@Override
	public Tuple tryRead(Tuple template) {
		Tuple returnedTuple = null;
		int i = 0;
		
		while (i<tuples.size() && returnedTuple == null) {
			if (tuples.get(i).matches(template)) {
				returnedTuple = tuples.get(i).deepclone();
			}
			i++;
		}
		
		return returnedTuple;
	}

	@Override
	public Collection<Tuple> takeAll(Tuple template) {
		List<Tuple> tuplesReturned = new ArrayList<Tuple>();
		
		lock.lock();
		
		for (Tuple t: tuples) {
			if (t.matches(template)) {
				tuplesReturned.add(t);
			}
		}
		
		lock.unlock();
		tuples.removeAll(tuplesReturned);
		
		return tuplesReturned;
	}

	@Override
	public Collection<Tuple> readAll(Tuple template) {
		List<Tuple> tuplesReturned = new ArrayList<Tuple>();
		
		for (Tuple t: tuples) {
			if (t.matches(template)) {
				tuplesReturned.add(t);
			}
		}
				
		return tuplesReturned;
	}

	@Override
	public void eventRegister(eventMode mode, eventTiming timing, Tuple template, Callback callback) {
		Tuple tmp ;
		if(timing == eventTiming.IMMEDIATE) {
			
			if(mode == eventMode.TAKE) {
				tmp = tryTake(template);
				
			} else {
				tmp = tryRead(template);
			}
			
			if (tmp != null) {
				// EXECUTION DU CALL BACK DANS UN NOUVEAU THREAD
				// MERCI LE GROUPE B
				new Thread() {
					public void run(){
						callback.call(tmp);
					}
				}.start();
				
			} else {
				saveCallEvent(template, mode, callback);
			}
			
		} else{
			saveCallEvent(template, mode, callback);
		}
	}

	@Override
	public void debug(String prefix) {
		System.out.println();
		System.out.println();
		System.out.print(prefix + " Tuples : [");
		for(Tuple t : tuples){
			System.out.print(t);
		}
		System.out.println("]");
		
		System.out.print(prefix + " CallEvents : [");
		for(Entry<Tuple, List<CallEvent>> entry : waitingCallEvents.entrySet()){
			System.out.print("{" + entry.getKey() + " -> " + entry.getValue().size() + "}");
		}
		System.out.println("]");		
		System.out.println();
		System.out.println();
	}
	
	/** Met un callback en attente sur le bon motif de tuple qu'il recherche
	 * @param Callback le callback en question
	 * @param eventMode l'eventMode du callback
	 * @param Tuple le motif du tuple
	 **/
	private void saveCallEvent(Tuple template, eventMode mode, Callback callback) {
		boolean found = false;
		CallEvent currCallEvent = new CallEvent(mode, template, callback);
		List<CallEvent> currWaitingCallEvents = null;
		
		
		Iterator<Entry<Tuple, List<CallEvent>>> it = waitingCallEvents.entrySet().iterator();
	    while (it.hasNext() && !found) {
	        Entry<Tuple,List<CallEvent>> pair = (Entry<Tuple, List<CallEvent>>) it.next();
	         
	        /* Si le template correspond à l'ID de la map */
	        if(template.matches(pair.getKey())) {
	        	found = true;
	        	currWaitingCallEvents = pair.getValue();
	        }
	    }
		if(currWaitingCallEvents != null) {
			currWaitingCallEvents.add(currCallEvent);
			
		} else {
			List<CallEvent> tmpList = new ArrayList<CallEvent>();
			
			tmpList.add(currCallEvent);
			
			waitingCallEvents.put(template,tmpList);
		}
	}
	
	/** A partir d'un motif, cherche le callEvent à appeler et l'appelle. 
	 * Si plusieurs callEvent sont présents pour le même motif, tant que l'eventMode
	 * des callback n'est pas eventMode.TAKE, on les appelle tous.
	 * @param Tuple
	 **/
	private void wakeUpCallBack(Tuple template) {
		
		boolean found = false;
		boolean take = false;
		List<CallEvent> currWaitingCallEvents = null;
		
		/* RECUPERATION DES CALL EVENT EN ATTENTE SI EXISTANT */
		
		Iterator<Entry<Tuple, List<CallEvent>>> it = waitingCallEvents.entrySet().iterator();
	    while (it.hasNext() && !found) {
	        Entry<Tuple,List<CallEvent>> pair = (Entry<Tuple, List<CallEvent>>) it.next();
	         
	        /* Si le template correspond à l'ID de la map */
	        if(template.matches(pair.getKey())) {
	        	found = true;
	        	currWaitingCallEvents = pair.getValue();
	        }
	    }


		/* SI CALL EVENTS EN ATTENTE ALORS ON REVEILLE TOUS LES READ JUSQU'AU PREMIER "TAKE" */
		if (currWaitingCallEvents != null) {
			int i = 0;
			while (i < currWaitingCallEvents.size() && !take) {
				CallEvent currCallEvent = currWaitingCallEvents.get(i);
				if (currCallEvent.getMode() == eventMode.READ) {
					
					// EXECUTION DU CALL BACK DANS UN NOUVEAU THREAD
					// MERCI LE GROUPE B
					new Thread() {
						public void run(){
							currCallEvent.getCallback().call(template);
						}
					}.start();
					//currCallEvent.getCallback().call(template);

				} else {
					take = true;
					
					// EXECUTION DU CALL BACK DANS UN NOUVEAU THREAD
					// MERCI LE GROUPE B
					new Thread() {
						public void run(){
							currCallEvent.getCallback().call(template);
						}
					}.start();
					
					tuples.remove(template);
				}
				i++;
			}
		}
	}
}
