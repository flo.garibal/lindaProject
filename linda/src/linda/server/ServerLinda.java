package linda.server;

import java.net.InetAddress;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import linda.Callback;
import linda.Tuple;
import linda.Linda.eventMode;
import linda.Linda.eventTiming;
import linda.shm.CallEventServer;

/** Shared memory implementation of Linda. */
public class ServerLinda extends UnicastRemoteObject implements LindaServer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -358288675389722274L;
	private List<Tuple> tuples;
	private Condition cond_take;
	private Condition cond_read;
	private Lock lock;
	private boolean lecteur;
	private Map<Tuple,List<CallEventServer>> waitingCallEventServers;
	
    public ServerLinda() throws RemoteException{
    	tuples = new ArrayList<Tuple>();
    	lock = new ReentrantLock();
    	cond_take = lock.newCondition();
    	cond_read = lock.newCondition();
    	lecteur = false;
    	waitingCallEventServers = new HashMap<Tuple,List<CallEventServer>>();
    }

	@Override
	public void write(Tuple t) {
		lock.lock();
		
		Tuple tmpTuple = t.deepclone();
		tuples.add(tmpTuple);
		
		lecteur = false;

		cond_read.signalAll();
		
		if (!lecteur) {
			cond_take.signal();
		}
		wakeUpCallBack(t);
		lock.unlock();
	}

	@Override
	public Tuple take(Tuple template) {
		Tuple returnedTuple = null;
		int i = 0;
		
		lock.lock();
		
		while (returnedTuple == null) {

			while (i<tuples.size() && returnedTuple == null) {
				if (tuples.get(i).matches(template)) {
					returnedTuple = tuples.get(i);
					tuples.remove(i);
				}
				i++;
			}
			if (returnedTuple == null) {
				try {
					cond_take.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			i = 0;
		}
		
		lock.unlock();
		
		return returnedTuple;
	}

	@Override
	public Tuple read(Tuple template) {
		lecteur = true;
		Tuple returnedTuple = null;
		int i = 0;
				
		lock.lock();
		
		while (returnedTuple == null) {
			while (i<tuples.size() && returnedTuple == null) {
				if (tuples.get(i).matches(template)) {
					returnedTuple = tuples.get(i).deepclone();
				}
				i++;
			}
			i = 0;
			if (returnedTuple == null) {
				try {
					cond_read.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		lock.unlock();
		
		return returnedTuple;
	}

	@Override
	public Tuple tryTake(Tuple template) {
		Tuple returnedTuple = null;
		int i = 0;
				
		while (i<tuples.size() && returnedTuple == null) {
			if (tuples.get(i).matches(template)) {
				returnedTuple = tuples.get(i);
				tuples.remove(i);
			}
			i++;
		}
		
		return returnedTuple;
	}

	@Override
	public Tuple tryRead(Tuple template) {
		Tuple returnedTuple = null;
		int i = 0;
		
		while (i<tuples.size() && returnedTuple == null) {
			if (tuples.get(i).matches(template)) {
				returnedTuple = tuples.get(i).deepclone();
			}
			i++;
		}
		
		return returnedTuple;
	}

	@Override
	public Collection<Tuple> takeAll(Tuple template) {
		List<Tuple> tuplesReturned = new ArrayList<Tuple>();
		
		lock.lock();
		
		for (Tuple t: tuples) {
			if (t.matches(template)) {
				tuplesReturned.add(t);
			}
		}
		
		lock.unlock();
		tuples.removeAll(tuplesReturned);
		
		return tuplesReturned;
	}

	@Override
	public Collection<Tuple> readAll(Tuple template) {
		List<Tuple> tuplesReturned = new ArrayList<Tuple>();
		
		for (Tuple t: tuples) {
			if (t.matches(template)) {
				tuplesReturned.add(t);
			}
		}
				
		return tuplesReturned;
	}

	@Override
	public void eventRegister(eventMode mode, eventTiming timing, Tuple template, String callback) {
		Tuple tmp ;
		if(timing == eventTiming.IMMEDIATE) {
			
			if(mode == eventMode.TAKE) {
				tmp = tryTake(template);
				
			} else {
				tmp = tryRead(template);
			}
			
			if (tmp != null) {
				try {
					((Callback) Class.forName(callback).newInstance()).call(tmp);
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				saveCallEventServer(template, mode, callback);
			}
			
		} else{
			saveCallEventServer(template, mode, callback);
		}
	}

	@Override
	public void debug(String prefix) {
		System.out.println();
		System.out.println();
		System.out.print(prefix + " Tuples : [");
		for(Tuple t : tuples){
			System.out.print(t);
		}
		System.out.println("]");
		
		System.out.print(prefix + " CallEventServers : [");
		for(Entry<Tuple, List<CallEventServer>> entry : waitingCallEventServers.entrySet()){
			System.out.print("{" + entry.getKey() + " -> " + entry.getValue().size() + "}");
		}
		System.out.println("]");		
		System.out.println();
		System.out.println();
	}
	
	/** Met un callback en attente sur le bon motif de tuple qu'il recherche
	 * @param Callback le callback en question
	 * @param eventMode l'eventMode du callback
	 * @param Tuple le motif du tuple
	 **/
	private void saveCallEventServer(Tuple template, eventMode mode, String callback) {
		boolean found = false;
		CallEventServer currCallEventServer = new CallEventServer(mode, template, callback);
		List<CallEventServer> currWaitingCallEventServers = null;
		
		
		Iterator<Entry<Tuple, List<CallEventServer>>> it = waitingCallEventServers.entrySet().iterator();
	    while (it.hasNext() && !found) {
	        Entry<Tuple,List<CallEventServer>> pair = (Entry<Tuple, List<CallEventServer>>) it.next();
	         
	        /* Si le template correspond à l'ID de la map */
	        if(template.matches(pair.getKey())) {
	        	found = true;
	        	currWaitingCallEventServers = pair.getValue();
	        }
	    }
		if(currWaitingCallEventServers != null) {
			currWaitingCallEventServers.add(currCallEventServer);
			
		} else {
			List<CallEventServer> tmpList = new ArrayList<CallEventServer>();
			
			tmpList.add(currCallEventServer);
			
			waitingCallEventServers.put(template,tmpList);
		}
	}
	
	/** A partir d'un motif, cherche le CallEventServer à appeler et l'appelle. 
	 * Si plusieurs CallEventServer sont présents pour le même motif, tant que l'eventMode
	 * des callback n'est pas eventMode.TAKE, on les appelle tous.
	 * @param Tuple
	 **/
	private void wakeUpCallBack(Tuple template) {
		
		boolean found = false;
		boolean take = false;
		List<CallEventServer> currWaitingCallEventServers = null;
		
		/* RECUPERATION DES CALL EVENT EN ATTENTE SI EXISTANT */
		
		Iterator<Entry<Tuple, List<CallEventServer>>> it = waitingCallEventServers.entrySet().iterator();
	    while (it.hasNext() && !found) {
	        Entry<Tuple,List<CallEventServer>> pair = (Entry<Tuple, List<CallEventServer>>) it.next();
	         
	        /* Si le template correspond à l'ID de la map */
	        if(template.matches(pair.getKey())) {
	        	found = true;
	        	currWaitingCallEventServers = pair.getValue();
	        }
	    }


		/* SI CALL EVENTS EN ATTENTE ALORS ON REVEILLE TOUS LES READ JUSQU'AU PREMIER "TAKE" */
		if (currWaitingCallEventServers != null) {
			int i = 0;
			while (i < currWaitingCallEventServers.size() && !take) {
				CallEventServer currCallEventServer = currWaitingCallEventServers.get(i);
				if (currCallEventServer.getMode() == eventMode.READ) {

					try {
						((Callback) Class.forName(currCallEventServer.getCallback()).newInstance()).call(template);
					} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {
					take = true;
					try {
						((Callback) Class.forName(currCallEventServer.getCallback()).newInstance()).call(template);
					} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					tuples.remove(template);
				}
				i++;
			}
		}
	}

	public static void main(String args[]){
		int port; String url;
		
		try{
			Integer I = new Integer(args[0]); port = I.intValue(); 
		}
		catch (Exception ex){
			System.out.println("Please enter: java ServerLinda <port>");return;
		}
		
		try {
			LocateRegistry.createRegistry(port);
			
			LindaServer obj = new ServerLinda();
			
			url = "//"+InetAddress.getLocalHost().getHostName()+":"+port+"/LindaServer";
			
			Naming.rebind(url, obj);
			System.out.println("Running on port "+port);
		}
		catch (Exception e) {
			System.out.println("An error occured during the launch, please try again.");
			e.printStackTrace();
		}
		
	}

	
}
