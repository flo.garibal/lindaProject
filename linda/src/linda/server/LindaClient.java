package linda.server;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import linda.Callback;
import linda.Linda;
import linda.Tuple;
import linda.shm.CallEvent;
import linda.shm.CallEventServer;

/** Client part of a client/server implementation of Linda.
 * It implements the Linda interface and propagates everything to the server it is connected to.
 * */
public class LindaClient implements Linda, Remote, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7415072759547746754L;
	private LindaServer linda;
	private int index;
	protected Map<Tuple,List<CallEvent>> waitingCallEvents;
	/** Initializes the Linda implementation.
     *  @param serverURI the URI of the server, e.g. "//localhost:4000/LindaServer".
     */
    public LindaClient(String serverURI) {
    	try {
			linda = (LindaServer) Naming.lookup(serverURI);
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	waitingCallEvents = new HashMap<Tuple, List<CallEvent>>();
    	System.out.println(waitingCallEvents);
    }
    
	@Override
	public void write(Tuple t) {
		// TODO Auto-generated method stub
		try {
			linda.write(t);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public Tuple take(Tuple template) {
		Tuple temp = null;
		try {
			temp = linda.take(template);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return temp;
	}

	@Override
	public Tuple read(Tuple template) {
		Tuple temp = null;
		try {
			temp = linda.read(template);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return temp;
	}

	@Override
	public Tuple tryTake(Tuple template) {
		Tuple temp = null;
		try {
			temp = linda.tryTake(template);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return temp;
	}

	@Override
	public Tuple tryRead(Tuple template) {
		Tuple temp = null;
		try {
			temp = linda.tryRead(template);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return temp;
	}

	@Override
	public Collection<Tuple> takeAll(Tuple template) {
		Collection<Tuple> temp = null;
		try {
			temp = linda.takeAll(template);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return temp;
	}

	@Override
	public Collection<Tuple> readAll(Tuple template) {
		Collection<Tuple> temp = null;
		try {
			temp = linda.readAll(template);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return temp;
	}

	@Override
	public void eventRegister(eventMode mode, eventTiming timing, Tuple template, Callback callback){
		try {
			linda.eventRegister(mode, timing, template, callback.getClass().getName());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	@Override
	public void debug(String prefix) {
		try {
			linda.debug(prefix);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	
}
