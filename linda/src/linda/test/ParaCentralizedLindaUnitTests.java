package linda.test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import linda.AsynchronousCallback;
import linda.Callback;
import linda.Linda;
import linda.Linda.eventMode;
import linda.Linda.eventTiming;
import linda.Tuple;

public class ParaCentralizedLindaUnitTests extends junit.framework.TestSuite {
	
	private final static int CONST = 9564;  
	private final static int nbTuplesInsere = 15000;
	private ArrayList<Tuple> tuples;
	private Tuple templ;
	private Linda linda;
	private Linda lindapar;
	private static boolean callbackCalled;
	private static Tuple tupleUsedByCallback;
	
	private static class CallbackFunction implements Callback {
        @Override
        public void call(Tuple t) {
            callbackCalled = true;
            tupleUsedByCallback = t;
        }
    }
	
	@Before
	public void setUp() {
		tuples = new ArrayList<Tuple>();
		for (int i=0 ; i<nbTuplesInsere; i++) {
			tuples.add(new Tuple(i, "test"+i));
		}
		
		// Template situé au milieu de la liste pour évaluer le temps de parcours de boucle 
		this.templ = new Tuple(CONST, String.class);
		this.linda = new linda.shm.CentralizedLinda();
		this.lindapar = new linda.shm.CentralizedLinda(); // CHANGER EN //
		this.callbackCalled = false;
		this.tupleUsedByCallback = null;
		
	}
	
	@Test
	public void testTakeBeforeTupleIn() {
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				for(int i=0; i<nbTuplesInsere; i++) {
					linda.write(tuples.get(i));
				}				
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// LANCEMENT CHRONO
		long time1 = System.currentTimeMillis();
		
		Tuple res = linda.take(templ);
		
		// RECUPERATION TEMPS 
		time1 = System.currentTimeMillis()-time1;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		assertTrue(new Tuple(CONST, "test"+CONST).contains(res));
		
		
		
		///////PARA  /////////
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					lindapar.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// LANCEMENT CHRONO
		long time2 = System.currentTimeMillis();
		
		Tuple respar = lindapar.take(templ);

		// RECUPERATION TEMPS CHRONO
		time2 = System.currentTimeMillis()- time2;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
		assertTrue(new Tuple(CONST, "test"+CONST).contains(respar));
		assertTrue(time2 <= time1);
	}
	
	@Test
	public void testTakeAfterTupleIn() {
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					linda.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// LANCEMENT CHRONO
		long time1 = System.currentTimeMillis();
		
		Tuple res = linda.take(templ);

		// RECUPERATION TEMPS
		time1 = System.currentTimeMillis() - time1;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		assertTrue(new Tuple(CONST, "test"+CONST).contains(res));
		
		/////////////     Par	///////////////////
		
		//Thread de dépôt dans le pool
				new Thread() {
					public void run() {
						//Attente avant le dépôt
						try {
							Thread.sleep(0);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						for(int i=0; i<nbTuplesInsere; i++) {
							lindapar.write(tuples.get(i));
						}
					}
				}.start();
				
				//Attente avant l'accès au pool
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				// LANCEMENT CHRONO
				long time2 = System.currentTimeMillis();
				
				Tuple respar = lindapar.take(templ);

				// RECUPERATION TEMPS
				time2 = System.currentTimeMillis() - time2;
				
				//Attente avant l'évaluation du résultat
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				assertTrue(new Tuple(CONST, "test"+CONST).contains(respar));
				assertTrue(time2 <= time1);
	}

	@Test
	public void testReadBeforeTupleIn() {
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					linda.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// LANCEMENT CHRONO
		long time1 = System.currentTimeMillis();
		
		Tuple res = linda.read(templ);
		
		// RECUPERATION TEMPS CHRONO
		time1 = System.currentTimeMillis() - time1;

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		assertTrue((new Tuple(CONST, "test"+CONST)).contains(res));

		
		/////////	PAR	//////////
		
		//Thread de dépôt dans le pool
				new Thread() {
					public void run() {
						//Attente avant le dépôt
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						for(int i=0; i<nbTuplesInsere; i++) {
							lindapar.write(tuples.get(i));
						}
					}
				}.start();
				
				//Attente avant l'accès au pool
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				// LANCEMENT CHRONO 
				long time2 = System.currentTimeMillis();
				
				Tuple respar = lindapar.read(templ);

				// RECUPERATION TEMPS CHRONO 
				time2 = System.currentTimeMillis() - time2;
				
				//Attente avant l'évaluation du résultat
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				assertTrue((new Tuple(CONST, "test"+CONST)).contains(respar));
				assertTrue(time2 <= time1);
	}

	@Test
	public void testReadAfterTupleIn() {
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					linda.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// LANCEMENT CHRONO
		long time1 = System.currentTimeMillis();
		
		Tuple res = linda.read(templ);

		// RECUPERATION TEMPS
		time1 = System.currentTimeMillis() - time1;
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue((new Tuple(CONST, "test"+CONST)).contains(res));
		
		
		/////	PAR	/////
		
		//Thread de dépôt dans le pool
				new Thread() {
					public void run() {
						//Attente avant le dépôt
						try {
							Thread.sleep(0);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						for(int i=0; i<nbTuplesInsere; i++) {
							lindapar.write(tuples.get(i));
						}
					}
				}.start();
				
				//Attente avant l'accès au pool
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				// LANCEMENT CHRONO
				long time2 = System.currentTimeMillis();
				
				Tuple respar = lindapar.read(templ);
				
				// RECUPERATION VALEUR CHRONO
				time2 = System.currentTimeMillis() - time2;

				//Attente avant l'évaluation du résultat
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				assertTrue((new Tuple(CONST, "test"+CONST)).contains(respar));
				assertTrue(time2 <= time1);
	}

	@Test
	public void testTryTakeBeforeTupleIn() {
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					linda.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// LANCEMENT CHRONO 
		long time1 = System.currentTimeMillis();
		
		Tuple res = linda.tryTake(templ);
		
		// RECUPERATION TEMPS CHRONO
		time1 = System.currentTimeMillis();

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertNull(res);
		
		
		////	PAR	///////
		
		//Thread de dépôt dans le pool
				new Thread() {
					public void run() {
						//Attente avant le dépôt
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						for(int i=0; i<nbTuplesInsere; i++) {
							lindapar.write(tuples.get(i));
						}
					}
				}.start();
				
				//Attente avant l'accès au pool
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				// LANCEMENT CHRONO
				long time2 = System.currentTimeMillis();
				
				Tuple respar = lindapar.tryTake(templ);
				
				// RELEVE DE TEMPS CHRONO
				time2 = System.currentTimeMillis() - time2;

				//Attente avant l'évaluation du résultat
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				assertNull(respar);
				assertTrue(time2 <= time1);
	}

	@Test
	public void testTryTakeAfterTupleIn() {
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					linda.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// LANCEMENT CHRONO
		long time1 = System.currentTimeMillis();
		
		Tuple res = linda.tryTake(templ);

		// RELEVE CHRONO
		time1 = System.currentTimeMillis() - time1;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(new Tuple(CONST, "test"+CONST).contains(res));
		
		
		//////		PAR		///////
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					lindapar.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// LANCEMENT CHRONO
		long time2 = System.currentTimeMillis();
		
		Tuple respar = lindapar.tryTake(templ);
		
		// RELEVE TEMPS CHRONO
		time2 = System.currentTimeMillis() - time2;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(new Tuple(CONST, "test"+CONST).contains(respar));
		assertTrue(time2 <= time1);
	}
	
	@Test
	public void testTryReadBeforeTupleIn() {
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					linda.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// LANCEMENT CHRONO 
		long time1 = System.currentTimeMillis();
		
		Tuple res = linda.tryRead(templ);

		// RECUPERATION RELEVE CHRONO 
		time1 = System.currentTimeMillis() - time1;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertNull(res);
		
		
		//////		PAR		/////
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					lindapar.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// LANCEMENT CHRONO
		long time2 = System.currentTimeMillis();
		
		Tuple respar = lindapar.tryRead(templ);

		// RELEVE CHRONO 
		time2 = System.currentTimeMillis() - time2;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertNull(respar);
		assertTrue(time2 <= time1);
	}

	@Test
	public void testTryReadAfterTupleIn() {
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					linda.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// LANCEMENT CHRONO
		long time1 = System.currentTimeMillis();
		
		Tuple res = linda.tryRead(templ);
		
		// RELEVE CHRONO 
		time1 = System.currentTimeMillis() - time1;

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(new Tuple(CONST, "test"+CONST).contains(res));
		
		
		///////		PAR		///////
		
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					lindapar.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// LANCEMENT CHRONO
		long time2 = System.currentTimeMillis();
		
		Tuple respar = lindapar.tryRead(templ);
		
		// RELEVE CHRONO
		time2 = System.currentTimeMillis() - time2;

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(new Tuple(CONST, "test"+CONST).contains(respar));
		assertTrue(time2 <= time1);
	}
	
	@Test
	public void testTakeAllAfterTupleIn() {

		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					linda.write(tuples.get(i));
				}
			}
		}.start();
		
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// LANCEMENT CHRONO
		long time1 = System.currentTimeMillis();
		
		Collection<Tuple> res = linda.takeAll(templ);

		// RELEVE CHRONO
		time1 = System.currentTimeMillis() - time1;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		boolean isCorrect = true;
		
		for(Tuple t : res) {
			isCorrect = isCorrect && new Tuple(CONST, "test"+CONST).contains(t);
		}
		
		assertTrue(isCorrect);
		
		
		
		//////		PAR		///////
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					lindapar.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// LANCEMENT CHRONO
		long time2 = System.currentTimeMillis();
		
		Collection<Tuple> respar = lindapar.takeAll(templ);

		// RELEVE CHRONO
		time2 = System.currentTimeMillis() - time2;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		boolean isCorrectpar = true;
		
		for(Tuple t : respar) {
			isCorrectpar = isCorrectpar && new Tuple(CONST, "test"+CONST).contains(t);
		}
		
		assertTrue(isCorrectpar);
		assertTrue(time2 <= time1);
	}

	@Test
	public void testTakeAllBeforeTupleIn() {
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					linda.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// LANCEMENT CHRONO 
		long time1 = System.currentTimeMillis();
		
		Collection<Tuple> res = linda.takeAll(templ);

		// RELEVE CHRONO
		time1 = System.currentTimeMillis() - time1;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(res.isEmpty());
		
		/////		PAR		//////
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					lindapar.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// LANCEMENT CHRONO
		long time2 = System.currentTimeMillis();
		
		Collection<Tuple> respar = lindapar.takeAll(templ);
		
		// RELEVE CHRONO
		time2 = System.currentTimeMillis() - time2;

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(respar.isEmpty());
		assertTrue(time2 <= time1);
	}
	
	@Test
	public void testReadAllAfterTupleIn() {
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					linda.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// LANCEMENT CHRONO
		long time1 = System.currentTimeMillis();
		
		Collection<Tuple> res = linda.readAll(templ);

		// RELEVE CHRONO
		time1 = System.currentTimeMillis() - time1;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		boolean isCorrect = true;
		
		for(Tuple t : res) {
			isCorrect = isCorrect && new Tuple(CONST, "test"+CONST).contains(t);
		}
		
		assertTrue(isCorrect);
		
		//////		PAR		///////		

		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					lindapar.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// LANCEMENT CHRONO
		long time2 = System.currentTimeMillis();
		
		Collection<Tuple> respar = lindapar.readAll(templ);

		// RELEVE CHRONO
		time2 = System.currentTimeMillis() - time2;
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		boolean isCorrectpar = true;
		
		for(Tuple t : respar) {
			isCorrectpar = isCorrectpar && new Tuple(CONST, "test"+CONST).contains(t);
		}
		
		assertTrue(isCorrectpar);
		assertTrue(time2 <= time1);
	}

	@Test
	public void testReadAllBeforeTupleIn() {

		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					linda.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// LANCEMENT CHRONO
		long time1 = System.currentTimeMillis();
		
		Collection<Tuple> res = linda.takeAll(templ);
		
		// RELEVE CHRONO
		time1 = System.currentTimeMillis() - time1;

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(res.isEmpty());
		
		
		///////		PAR		////////
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					lindapar.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// LANCEMENT CHRONO
		long time2 = System.currentTimeMillis();
		
		Collection<Tuple> respar = lindapar.takeAll(templ);

		// RELEVE CHRONO 
		time2 = System.currentTimeMillis() - time2;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(respar.isEmpty());
		assertTrue(time2 <= time1);
	}
	
	@Test
	public void testEventRegisterTakeBeforeTupleIn() {
		long time1 = System.currentTimeMillis();
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					linda.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		linda.eventRegister(eventMode.TAKE, eventTiming.IMMEDIATE, templ, new AsynchronousCallback(new CallbackFunction()));
		
		boolean notYetCalled = !callbackCalled;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(new Tuple(CONST, "test"+CONST).contains(tupleUsedByCallback) && notYetCalled && callbackCalled);
		
		
		
		
		/////		PAR		//////
		
		
		long time2 = System.currentTimeMillis();
		//Thread de dépôt dans le pool
				new Thread() {
					public void run() {
						//Attente avant le dépôt
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						for(int i=0; i<nbTuplesInsere; i++) {
							lindapar.write(tuples.get(i));
						}
					}
				}.start();
				
				//Attente avant l'accès au pool
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				lindapar.eventRegister(eventMode.TAKE, eventTiming.IMMEDIATE, templ, new AsynchronousCallback(new CallbackFunction()));
				
				boolean notYetCalledpar = !callbackCalled;
				
				//Attente avant l'évaluation du résultat
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				assertTrue(new Tuple(CONST, "test"+CONST).contains(tupleUsedByCallback) && notYetCalledpar && callbackCalled);
				assertTrue((System.currentTimeMillis() - time2) <= time2 - time1);
	}
	
	@Test
	public void testEventRegisterTakeAfterTupleIn() {
		long time1 = System.currentTimeMillis();
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					linda.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		linda.eventRegister(eventMode.TAKE, eventTiming.IMMEDIATE, templ, new AsynchronousCallback(new CallbackFunction()));
		
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		boolean calledAlready = callbackCalled;
		Tuple tupleUsed = tupleUsedByCallback;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(new Tuple(CONST, "test"+CONST).contains(tupleUsed) && calledAlready);
		
		
		
		
		///////		PAR		////////
		
		long time2 = System.currentTimeMillis();
		//Thread de dépôt dans le pool
				new Thread() {
					public void run() {
						//Attente avant le dépôt
						try {
							Thread.sleep(0);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						for(int i=0; i<nbTuplesInsere; i++) {
							lindapar.write(tuples.get(i));
						}
					}
				}.start();
				
				//Attente avant l'accès au pool
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				lindapar.eventRegister(eventMode.TAKE, eventTiming.IMMEDIATE, templ, new AsynchronousCallback(new CallbackFunction()));
				
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				boolean calledAlreadypar = callbackCalled;
				Tuple tupleUsedpar = tupleUsedByCallback;
				
				//Attente avant l'évaluation du résultat
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				assertTrue(new Tuple(CONST, "test"+CONST).contains(tupleUsedpar) && calledAlreadypar);
				assertTrue((System.currentTimeMillis() - time2) <= time2 - time1);
	}

	@Test
	public void testEventRegisterReadBeforeTupleIn() {
		long time1 = System.currentTimeMillis();
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					linda.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		linda.eventRegister(eventMode.READ, eventTiming.IMMEDIATE, templ, new AsynchronousCallback(new CallbackFunction()));
		
		boolean notYetCalled = !callbackCalled;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(new Tuple(CONST, "test"+CONST).contains(tupleUsedByCallback) && notYetCalled && callbackCalled);
		
		
		
		////////		PAR		///////
		
		
		long time2 = System.currentTimeMillis();
		//Thread de dépôt dans le pool
				new Thread() {
					public void run() {
						//Attente avant le dépôt
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						for(int i=0; i<nbTuplesInsere; i++) {
							lindapar.write(tuples.get(i));
						}
					}
				}.start();
				
				//Attente avant l'accès au pool
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				lindapar.eventRegister(eventMode.READ, eventTiming.IMMEDIATE, templ, new AsynchronousCallback(new CallbackFunction()));
				
				boolean notYetCalledpar = !callbackCalled;
				
				//Attente avant l'évaluation du résultat
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				assertTrue(new Tuple(CONST, "test"+CONST).contains(tupleUsedByCallback) && notYetCalledpar && callbackCalled);
				assertTrue((System.currentTimeMillis() - time2) <= time2 - time1);
	}
	
	@Test
	public void testEventRegisterReadAfterTupleIn() {
		long time1 = System.currentTimeMillis();
		
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for(int i=0; i<nbTuplesInsere; i++) {
					linda.write(tuples.get(i));
				}
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		linda.eventRegister(eventMode.READ, eventTiming.IMMEDIATE, templ, new AsynchronousCallback(new CallbackFunction()));
		
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		boolean calledAlready = callbackCalled;
		Tuple tupleUsed = tupleUsedByCallback;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(new Tuple(CONST, "test"+CONST).contains(tupleUsed) && calledAlready);
		
		
		//////			PAR			///////
		
		long time2 = System.currentTimeMillis();
		//Thread de dépôt dans le pool
				new Thread() {
					public void run() {
						//Attente avant le dépôt
						try {
							Thread.sleep(0);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						for(int i=0; i<nbTuplesInsere; i++) {
							lindapar.write(tuples.get(i));
						}
					}
				}.start();
				
				//Attente avant l'accès au pool
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				lindapar.eventRegister(eventMode.READ, eventTiming.IMMEDIATE, templ, new AsynchronousCallback(new CallbackFunction()));
				
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				boolean calledAlreadypar = callbackCalled;
				Tuple tupleUsedpar = tupleUsedByCallback;
				
				//Attente avant l'évaluation du résultat
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				assertTrue(new Tuple(CONST, "test"+CONST).contains(tupleUsedpar) && calledAlreadypar);
				assertTrue((System.currentTimeMillis() - time2) <= time2 - time1);
	}

}
