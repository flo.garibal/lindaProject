package linda.test;

import java.util.Collection;

import linda.AsynchronousCallback;
import linda.Callback;
import linda.Linda;
import linda.Tuple;
import linda.Linda.eventMode;
import linda.Linda.eventTiming;
import linda.shm.CentralizedLinda;

public class TestCentralizedLinda {
	private static class TestCallback1 implements Callback {
        @Override
        public void call(Tuple t) {
            System.out.println("== Execution du Callback 1");
            System.out.println("== Tuple utilisé : " + t);
        }
    }
    
	private static class TestCallback2 implements Callback {
        @Override
        public void call(Tuple t) {
            System.out.println("== Execution du Callback 2");
            System.out.println("== Tuple utilisé : " + t);
        }
    }
	
	public static void main(String[] args) {
		
		
		
		final Linda lindaTest = new linda.shm.CentralizedLinda();
		
		//thread effectuant les opérations d'accès au pool de tuples
		new Thread() {
			public void run() {
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				// take bloquant d'un tuple pas encore présent
				presentationTest(1, "Take Bloquant");
				Tuple templ1 = new Tuple(Integer.class, String.class);
				
				//on essaie de prendre un tuple correspondant
				//il n'est pas présent à l'appel de take, un autre
				//thread l'écrit quelques secondes plus tard
				lancementTest(1, "En attente d'un dépôt d'un tuple correspondant au motif");
				Tuple res1 = lindaTest.take(templ1);
				testReussi(1, "[ 1 \"testtake\" ]", res1);
				lindaTest.debug("== ");
				separateurTest();
				
				// read bloquant d'un tuple pas encore présent
				presentationTest(2, "Read Bloquant");
				Tuple templ2 = new Tuple(Integer.class, String.class);
				
				//on essaie de prendre un tuple correspondant
				//il n'est pas présent à l'appel de read, un autre
				//thread l'écrit quelques secondes plus tard
				lancementTest(2, "En attente d'un dépôt d'un tuple correspondant au motif");
				Tuple res2 = lindaTest.read(templ2);
				testReussi(2, "[ 2 \"testread\" ]", res2);
				lindaTest.debug("== ");
				separateurTest();
				
				//on retire le tuple présent dans le pool
				System.out.println("");
				System.out.println("");
				separateurTest();
				
				System.out.println("== Vidage du pool de tuple");
				System.out.println("==");
				System.out.println("== Etat du pool avant :");
				lindaTest.debug("== ");
				lindaTest.take(templ2);
				System.out.println("== Etat du pool après :");
				lindaTest.debug("== ");
				separateurTest();
				
				// tryTake d'un tuple pas encore présent
				presentationTest(3, "TryTake Non Bloquant");
				Tuple templ3 = new Tuple(Integer.class, String.class);
				
				//on essaie de prendre un tuple correspondant
				//il n'est pas présent, on devrait avoir une collection vide
				lancementTest(3, "TryTake sans tuple correspondant");
				Tuple res3 = lindaTest.tryTake(templ3);
				testReussi(3, "null", res3);
				lindaTest.debug("== ");
				separateurTest();
				
				//on attend l'ajout d'un tuple pour le tryTake
				try {
					Thread.sleep(4000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				// tryTake d'un tuple présent
				presentationTest(4, "TryTake Non Bloquant");
				Tuple templ4 = new Tuple(Integer.class, String.class);
				
				//on essaie de prendre un tuple correspondant
				//il estprésent, on devrait le retourner
				lancementTest(4, "TryTake avec tuple correspondant");
				Tuple res4 = lindaTest.tryTake(templ4);
				testReussi(4, "[ 4 \"testtryTake\" ]", res4);
				lindaTest.debug("== ");
				separateurTest();
				
				// tryRead d'un tuple pas encore présent
				presentationTest(5, "TryRead Non Bloquant");
				Tuple templ5 = new Tuple(Integer.class, String.class);
				
				//on essaie de lire un tuple correspondant
				//il n'est pas présent, on devrait avoir une collection vide
				lancementTest(5, "TryRead sans tuple correspondant");
				Tuple res5 = lindaTest.tryTake(templ5);
				testReussi(5, "null", res5);
				lindaTest.debug("== ");
				separateurTest();
				
				//on attend l'ajout d'un tuple pour le tryTake
				try {
					Thread.sleep(4000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				// tryRead d'un tuple présent
				presentationTest(6, "TryRead Non Bloquant");
				Tuple templ6 = new Tuple(Integer.class, String.class);
				
				//on essaie de lire un tuple correspondant
				//il estprésent, on devrait le retourner
				lancementTest(6, "TryRead avec tuple correspondant");
				Tuple res6 = lindaTest.tryTake(templ6);
				testReussi(6, "[ 6 \"testtryRead\" ]", res6);
				lindaTest.debug("== ");
				separateurTest();
				
				//takeAll d'un tuple pas encore présent
				presentationTest(7, "TakeAll Non Bloquant sans tuple");
				Tuple templ7 = new Tuple(Integer.class, String.class);
				
				//on essaie de prendre un pattern tuple correspondant
				//il n'est pas présent, on devrait avoir une collection vide
				lancementTest(7, "TakeAll sans tuple correspondant");
				Collection<Tuple> res7 = lindaTest.takeAll(templ7);
				testReussi(7, "[]", res7);
				lindaTest.debug("== ");
				separateurTest();
				
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//takeAll d'un tuple présent
				presentationTest(8, "TakeAll Non Bloquant avec tuple");
				Tuple templ8 = new Tuple(Integer.class, String.class);
				
				//on essaie de prendre un pattern tuple correspondant
				//il est présent, on devrait avoir une collection avec trois fois le tuple
				lancementTest(8, "TakeAll sans tuple correspondant");
				Collection<Tuple> res8 = lindaTest.takeAll(templ8);
				testReussi(8, "[[ 8 \"testtakeall\" ], [ 8 \"testtakeall\" ], [ 8 \"testtakeall\" ]]", res8);
				lindaTest.debug("== ");
				separateurTest();
				
				
				//readAll d'un tuple pas encore présent
				presentationTest(9, "ReadAll Non Bloquant sans tuple");
				Tuple templ9 = new Tuple(Integer.class, String.class);
				
				//on essaie de lire un pattern tuple correspondant
				//il n'est pas présent, on devrait avoir une collection vide
				lancementTest(9, "TakeAll sans tuple correspondant");
				Collection<Tuple> res9 = lindaTest.readAll(templ9);
				testReussi(9, "[]", res9);
				lindaTest.debug("== ");
				separateurTest();
				
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//takeAll d'un tuple présent
				presentationTest(10, "ReadAll Non Bloquant avec tuple");
				Tuple templ10 = new Tuple(Integer.class, String.class);
				
				//on essaie de lire un pattern tuple correspondant
				//il est présent, on devrait avoir une collection avec trois fois le tuple
				lancementTest(10, "TakeAll sans tuple correspondant");
				Collection<Tuple> res10 = lindaTest.readAll(templ10);
				testReussi(10, "[[ 10 \"testreadall\" ], [ 10 \"testreadall\" ], [ 10 \"testreadall\" ]]", res10);
				lindaTest.debug("== ");
				separateurTest();
				
				//on retire les tuples présents dans le pool
				System.out.println("");
				System.out.println("");
				separateurTest();
				
				System.out.println("== Vidage du pool de tuple");
				System.out.println("==");
				System.out.println("== Etat du pool avant :");
				lindaTest.debug("== ");
				lindaTest.takeAll(templ2);
				System.out.println("== Etat du pool après :");
				lindaTest.debug("== ");
				separateurTest();
				
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//eventRegister immédiat avec un tuple présent
				presentationTest(11, "EventRegister immédiat avec tuple");
				Tuple templ11 = new Tuple(Integer.class, String.class);
				
				lancementTest(11, "EventRegister avec tuple présent, immédiat");
				lindaTest.eventRegister(eventMode.TAKE, eventTiming.IMMEDIATE, templ11, new AsynchronousCallback(new TestCallback1()));
				
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				presentationTest(12, "EventRegister immédiat avec tuple");
				Tuple templ12 = new Tuple(Integer.class, String.class);
				
				lancementTest(12, "EventRegister avec tuple présent, immédiat");
				lindaTest.eventRegister(eventMode.TAKE, eventTiming.IMMEDIATE, templ12, new AsynchronousCallback(new TestCallback2()));
				
				
			}
			
		}.start();
		
		
		//thread effectuant les écritures dans le pool de tuples pour les take
		new Thread() {
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Tuple t1 = new Tuple(1, "testtake");
				lindaTest.write(t1);
			}
		}.start();
			
		
		//thread effectuant les écritures dans le pool de tuples pour les read
		new Thread() {
			public void run() {
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Tuple t2 = new Tuple(2, "testread");
				lindaTest.write(t2);
			}
		}.start();
		
		
		//thread effectuant les écritures dans le pool de tuples pour les trytake
		new Thread() {
			public void run() {
				try {
					Thread.sleep(6000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Tuple t4 = new Tuple(4, "testtryTake");
				lindaTest.write(t4);
			}
		}.start();
			
		//thread effectuant les écritures dans le pool de tuples pour les tryread
		new Thread() {
			public void run() {
				try {
					Thread.sleep(9000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Tuple t6 = new Tuple(6, "testtryRead");
				lindaTest.write(t6);
			}
		}.start();
		
		//thread effectuant les écritures dans le pool de tuples pour les takeall
		new Thread() {
			public void run() {
				try {
					Thread.sleep(12000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Tuple t8 = new Tuple(8, "testtakeall");
				lindaTest.write(t8);
				lindaTest.write(t8);
				lindaTest.write(t8);
			}
		}.start();
	
			
		//thread effectuant les écritures dans le pool de tuples pour les readall
		new Thread() {
			public void run() {
				try {
					Thread.sleep(15000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Tuple t10 = new Tuple(10, "testreadall");
				lindaTest.write(t10);
				lindaTest.write(t10);
				lindaTest.write(t10);
			}
		}.start();
		
		//thread effectuant les écritures dans le pool de tuples pour les eventRegister immédiats
		new Thread() {
			public void run() {
				try {
					Thread.sleep(18000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Tuple t11 = new Tuple(11, "testeventregisterimmediat");
				lindaTest.write(t11);
			}
		}.start();
		//thread effectuant les écritures dans le pool de tuples pour les eventRegister immédiats
		new Thread() {
			public void run() {
				try {
					Thread.sleep(24000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Tuple t12 = new Tuple(12, "testeventregisterfutur");
				lindaTest.write(t12);
			}
		}.start();
	}
	
	private static void presentationTest(int testNumber, String testName) {
		System.out.println("");
		System.out.println("");
		
		separateurTest();
		System.out.println("== Test number : " + testNumber);
		System.out.println("==   Test name : " + testName);
	}
	
	private static void lancementTest(int testNumber, String testContent) {
		System.out.println("==");
		System.out.println("== Test " + testNumber + " lancé");
		System.out.println("== " + testContent);
	}
	
	private static void testReussi(int testNumber, String resultatAttendu, Object resultatReel) {
		System.out.println("==");
		
		System.out.println("== Test " + testNumber + " réussi");
		System.out.println("== Résultat attendu : " + resultatAttendu);
		if (resultatReel != null) {
			System.out.println("==    Résultat réél : " + resultatReel.toString());
		}
		else {
			System.out.println("==    Résultat réél : " + "null");
		}
		System.out.println("==");
		System.out.println("== Etat du pool : ");
	}
	
	private static void separateurTest() {
		System.out.println("======================================================================");
	}
}
