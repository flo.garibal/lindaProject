package linda.test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import linda.AsynchronousCallback;
import linda.Callback;
import linda.Linda;
import linda.Linda.eventMode;
import linda.Linda.eventTiming;
import linda.Tuple;

public class CentralizedLindaUnitTests extends junit.framework.TestSuite {
	private Tuple tuple;
	private Tuple templ;
	private Linda linda;
	private static boolean callbackCalled;
	private static Tuple tupleUsedByCallback;
	
	private static class CallbackFunction implements Callback {
        @Override
        public void call(Tuple t) {
            callbackCalled = true;
            tupleUsedByCallback = t;
        }
    }
	
	@Before
	public void setUp() {
		this.tuple = new Tuple(1, "test");
		this.templ = new Tuple(Integer.class, String.class);
		this.linda = new linda.shm.CentralizedLinda();
		this.callbackCalled = false;
		this.tupleUsedByCallback = null;
		
	}
	
	@Test
	public void testTakeBeforeTupleIn() {
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				linda.write(tuple);
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Tuple res = linda.take(templ);

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(new Tuple(1, "test").contains(res));
	}

	@Test
	public void testTakeAfterTupleIn() {
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				linda.write(tuple);
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Tuple res = linda.take(templ);

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(new Tuple(1, "test").contains(res));
	}

	@Test
	public void testReadBeforeTupleIn() {
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				linda.write(tuple);
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Tuple res = linda.read(templ);

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue((new Tuple(1, "test")).contains(res));
	}

	@Test
	public void testReadAfterTupleIn() {
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				linda.write(tuple);
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Tuple res = linda.read(templ);

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue((new Tuple(1, "test")).contains(res));
	}

	@Test
	public void testTryTakeBeforeTupleIn() {
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				linda.write(tuple);
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Tuple res = linda.tryTake(templ);

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertNull(res);
	}

	@Test
	public void testTryTakeAfterTupleIn() {
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				linda.write(tuple);
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Tuple res = linda.tryTake(templ);

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(new Tuple(1, "test").contains(res));
	}
	
	@Test
	public void testTryReadBeforeTupleIn() {
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				linda.write(tuple);
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Tuple res = linda.tryRead(templ);

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertNull(res);
	}

	@Test
	public void testTryReadAfterTupleIn() {
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				linda.write(tuple);
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Tuple res = linda.tryRead(templ);

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(new Tuple(1, "test").contains(res));
	}
	
	@Test
	public void testTakeAllAfterTupleIn() {

		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				linda.write(tuple);
				linda.write(tuple);
				linda.write(tuple);
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Collection<Tuple> res = linda.takeAll(templ);

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		boolean isCorrect = true;
		
		for(Tuple t : res) {
			isCorrect = isCorrect && new Tuple(1, "test").contains(t);
		}
		
		assertTrue(isCorrect);
	}

	@Test
	public void testTakeAllBeforeTupleIn() {
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				linda.write(tuple);
				linda.write(tuple);
				linda.write(tuple);
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Collection<Tuple> res = linda.takeAll(templ);

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(res.isEmpty());
	}
	
	@Test
	public void testReadAllAfterTupleIn() {
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				linda.write(tuple);
				linda.write(tuple);
				linda.write(tuple);
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Collection<Tuple> res = linda.readAll(templ);

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		boolean isCorrect = true;
		
		for(Tuple t : res) {
			isCorrect = isCorrect && new Tuple(1, "test").contains(t);
		}
		
		assertTrue(isCorrect);
	}

	@Test
	public void testReadAllBeforeTupleIn() {
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				linda.write(tuple);
				linda.write(tuple);
				linda.write(tuple);
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Collection<Tuple> res = linda.takeAll(templ);

		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(res.isEmpty());
	}
	
	@Test
	public void testEventRegisterTakeBeforeTupleIn() {
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				linda.write(tuple);
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		linda.eventRegister(eventMode.TAKE, eventTiming.IMMEDIATE, templ, new AsynchronousCallback(new CallbackFunction()));
		
		boolean notYetCalled = !callbackCalled;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(new Tuple(1, "test").contains(tupleUsedByCallback) && notYetCalled && callbackCalled);
	}
	
	@Test
	public void testEventRegisterTakeAfterTupleIn() {
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				linda.write(tuple);
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		linda.eventRegister(eventMode.TAKE, eventTiming.IMMEDIATE, templ, new AsynchronousCallback(new CallbackFunction()));
		
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		boolean calledAlready = callbackCalled;
		Tuple tupleUsed = tupleUsedByCallback;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(new Tuple(1, "test").contains(tupleUsed) && calledAlready);
	}

	@Test
	public void testEventRegisterReadBeforeTupleIn() {
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				linda.write(tuple);
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		linda.eventRegister(eventMode.READ, eventTiming.IMMEDIATE, templ, new AsynchronousCallback(new CallbackFunction()));
		
		boolean notYetCalled = !callbackCalled;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(new Tuple(1, "test").contains(tupleUsedByCallback) && notYetCalled && callbackCalled);
	}
	
	@Test
	public void testEventRegisterReadAfterTupleIn() {
		//Thread de dépôt dans le pool
		new Thread() {
			public void run() {
				//Attente avant le dépôt
				try {
					Thread.sleep(0);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				linda.write(tuple);
			}
		}.start();
		
		//Attente avant l'accès au pool
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		linda.eventRegister(eventMode.READ, eventTiming.IMMEDIATE, templ, new AsynchronousCallback(new CallbackFunction()));
		
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		boolean calledAlready = callbackCalled;
		Tuple tupleUsed = tupleUsedByCallback;
		
		//Attente avant l'évaluation du résultat
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertTrue(new Tuple(1, "test").contains(tupleUsed) && calledAlready);
	}

}
