// Time-stamp: <08 déc 2009 08:30 queinnec@enseeiht.fr>
package linda.applications.philo;
import linda.outils.SemaphoreLinda;

public class PhiloSemLinda implements StrategiePhilo {

    /****************************************************************/
	private SemaphoreLinda[] fourchette;
	private SemaphoreLinda table;
	
    public PhiloSemLinda (int nbPhilosophes) {
    	this.fourchette = new SemaphoreLinda[nbPhilosophes];
    	for (int i=0;i<nbPhilosophes;i++){
    		fourchette[i] = new SemaphoreLinda("Fourchette"+i,1);
    	}
    	this.table = new SemaphoreLinda("Table",1);
    }

    /** Le philosophe no demande les fourchettes.
     *  Précondition : il n'en possède aucune.
     *  Postcondition : quand cette méthode retourne, il possède les deux fourchettes adjacentes à son assiette. */
    public void demanderFourchettes (int no) throws InterruptedException
    {
    	int fg = Main.FourchetteGauche(no);
    	int fd = Main.FourchetteDroite(no);
    	this.table.acquire();
    	this.fourchette[fg].acquire();
    	IHMPhilo.poser(fg, EtatFourchette.AssietteDroite);
    	this.fourchette[fd].acquire();
    	IHMPhilo.poser(fd, EtatFourchette.AssietteGauche);
    	this.table.release();
    }

    /** Le philosophe no rend les fourchettes.
     *  Précondition : il possède les deux fourchettes adjacentes à son assiette.
     *  Postcondition : il n'en possède aucune. Les fourchettes peuvent être libres ou réattribuées à un autre philosophe. */
    public void libererFourchettes (int no)
    {
    	int fg = Main.FourchetteGauche(no);
    	int fd = Main.FourchetteDroite(no);
    	IHMPhilo.poser(fg, EtatFourchette.Table);
    	IHMPhilo.poser(fd, EtatFourchette.Table);

    	this.fourchette[fg].release();
    	this.fourchette[fd].release();
    }

    /** Nom de cette stratégie (pour la fenêtre d'affichage). */
    public String nom() {
        return "Implantation Sémaphores, stratégie n°1";
    }

}

