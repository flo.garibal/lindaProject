// Time-stamp: <08 déc 2009 08:30 queinnec@enseeiht.fr>
package linda.applications.philo;
import java.util.concurrent.Semaphore;

import linda.outils.SemaphoreLinda;

public class PhiloSemLinda2 implements StrategiePhilo {

	private SemaphoreLinda protect;
    private SemaphoreLinda[] semPhilo;
    private EtatPhilosophe[] etat;
    
    
    public PhiloSemLinda2(int nbPhilosophes){
            protect = new SemaphoreLinda("Protect",1);
            semPhilo = new SemaphoreLinda[nbPhilosophes];
            etat = new EtatPhilosophe[nbPhilosophes];
            for (int i = 0;i<nbPhilosophes;i++){
                    etat[i] = EtatPhilosophe.Pense;
                    semPhilo[i] = new SemaphoreLinda("Philosophe"+i,1);
            }
    }
    
    public boolean peutManger(int no){
            return ((etat[no]==EtatPhilosophe.Demande)&&(etat[Main.PhiloDroite(no)]!=EtatPhilosophe.Mange))&&(etat[Main.PhiloGauche(no)]!=EtatPhilosophe.Mange);
    }
    
    /* (non-Javadoc)
     * @see StrategiePhilo#demanderFourchettes(int)
     */
    @Override
    public void demanderFourchettes(int no) throws InterruptedException {
            protect.acquire();
            if (peutManger(no)) {
                    etat[no] = EtatPhilosophe.Mange;
                    semPhilo[no].release();
            }
            else {
                    etat[no] = EtatPhilosophe.Demande;
            }
            protect.release();
            semPhilo[no].acquire();
    }

    /* (non-Javadoc)
     * @see StrategiePhilo#libererFourchettes(int)
     */
    @Override
    public void libererFourchettes(int no) throws InterruptedException {
            // TODO Auto-generated method stub
            protect.acquire();
            etat[no] = EtatPhilosophe.Pense;
            if (etat[Main.PhiloGauche(no)]==EtatPhilosophe.Demande && peutManger(Main.PhiloGauche(no))){
                    etat[Main.PhiloGauche(no)] = EtatPhilosophe.Mange;
                    semPhilo[Main.PhiloGauche(no)].release();
            }
            if (etat[Main.PhiloDroite(no)]==EtatPhilosophe.Demande && peutManger(Main.PhiloDroite(no))){
                    etat[Main.PhiloDroite(no)] = EtatPhilosophe.Mange;
                    semPhilo[Main.PhiloDroite(no)].release();
            }
            protect.release();
    }

    /* (non-Javadoc)
     * @see StrategiePhilo#nom()
     */
    @Override
    public String nom() {
            // TODO Auto-generated method stub
            return null;
    }


}

