// Time-stamp: <08 déc 2009 08:30 queinnec@enseeiht.fr>
package linda.applications.philo;

import java.util.concurrent.Semaphore;

public class PhiloSem implements StrategiePhilo {
	
	private Semaphore[] fourchette;
	private EtatPhilosophe[] etat;
	private Semaphore protect;

    /****************************************************************/

    public PhiloSem (int nbPhilosophes) {
    	fourchette = new Semaphore[nbPhilosophes];
     	for (int i = 0 ; i < nbPhilosophes; i++) {
    			fourchette[i] = new Semaphore(1);
    			etat[i] =  EtatPhilosophe.Pense;
    	}
    	protect = new Semaphore(1);
    }

    /** Le philosophe no demande les fourchettes.
     *  Précondition : il n'en possède aucune.
     *  Postcondition : quand cette méthode retourne, il possède les deux fourchettes adjacentes à son assiette. */
    public void demanderFourchettes (int no) throws InterruptedException
    {
    	int fg = Main.FourchetteGauche(no);
    	int fd = Main.FourchetteDroite(no);
    	
    	protect.acquire();
    	
    	if (peutManger(no)){
    		    	
    		fourchette[fg].acquire();
    		fourchette[fd].acquire();
    		etat[no] = EtatPhilosophe.Mange;
    		IHMPhilo.poser(no, EtatFourchette.AssietteDroite);
    		// Thread.sleep(1000); /*Bloqué */
    		//fourchette[fd].acquire();/*solution non perf */
    		IHMPhilo.poser(no, EtatFourchette.AssietteGauche);
    	}else {
    		etat[no] = EtatPhilosophe.Demande;
    	}
    	
    	protect.release();
    }

    /** Le philosophe no rend les fourchettes.
     *  Précondition : il possède les deux fourchettes adjacentes à son assiette.
     *  Postcondition : il n'en possède aucune. Les fourchettes peuvent être libres ou réattribuées à un autre philosophe. 
     * @throws InterruptedException */
    public void libererFourchettes (int no) throws InterruptedException
    {
    	int fg = Main.FourchetteGauche(no);
    	int fd = Main.FourchetteDroite(no);
    	
    	protect.acquire();
    	
    	if (etat[fg] == EtatPhilosophe.Demande && peutManger(fg) ){
        	etat[fg] = EtatPhilosophe.Mange; 
    	}
    	if (etat[fd] == EtatPhilosophe.Demande && peutManger(fd) ){
        	etat[fd] = EtatPhilosophe.Mange; 
    	}    	
    	
    	IHMPhilo.poser(no, EtatFourchette.Table);
    	IHMPhilo.poser(no, EtatFourchette.Table);
    	fourchette[fg].release();
    	fourchette [fd].release();
    	
    	etat[no] = EtatPhilosophe.Pense;
    	
    	protect.release();
    }
    
    public boolean peutManger(int no){
    	return ( etat[no-1] != EtatPhilosophe.Mange && etat[no + 1] != EtatPhilosophe.Mange && etat[no] == EtatPhilosophe.Demande ) ;
    }

    /** Nom de cette stratégie (pour la fenêtre d'affichage). */
    public String nom() {
        return "Implantation Sémaphores, stratégie ???";
    }

}