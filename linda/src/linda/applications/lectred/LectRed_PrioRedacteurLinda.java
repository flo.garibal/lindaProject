package linda.applications.lectred;
// Time-stamp: <08 Apr 2008 11:35 queinnec@enseeiht.fr>

import linda.outils.BarriereLinda;
import linda.outils.SemaphoreLinda;

/** Lecteurs/rédacteurs
 * stratégie d'ordonnancement: priorité aux rédacteurs,
 * implantation: avec un moniteur. */
public class LectRed_PrioRedacteurLinda implements LectRed
{
	private int nbLect, attenteRed;
	private boolean Red;
	private SemaphoreLinda ready, lect, red;
	
    public LectRed_PrioRedacteurLinda()
    {
    	this.nbLect = 0;
    	this.attenteRed = 0;
    	this.Red=false;
    	this.ready = new SemaphoreLinda("Ready", 1);
    	this.lect = new SemaphoreLinda("Lect", 1);
    	this.red = new SemaphoreLinda("Red", 1);
    }

    public void demanderLecture() throws InterruptedException
    {
    	ready.acquire();
    	while (Red || attenteRed > 0) {
    		ready.release();
    		lect.acquire();
    		ready.acquire();
    	}
    	nbLect++;
    	ready.release();
    	lect.release();
    }

    public void terminerLecture() throws InterruptedException
    {
    	ready.acquire();
    	nbLect--;
    	if (nbLect==0) {
    		red.release();
    	}
    	ready.release();
    }

    public void demanderEcriture() throws InterruptedException
    {
    	ready.acquire();
		attenteRed++;
    	while (Red || nbLect > 0) {
    		ready.release();
        	red.acquire();;
        	ready.acquire();
    	}
    	attenteRed--;
    	Red= true;
    	ready.release();
    }

    public void terminerEcriture() throws InterruptedException
    {
    	ready.acquire();
    	Red=false;
    	if (attenteRed > 0){
    		red.release();
    	}
    	else {
    		lect.release();
    	}
    	ready.release();
    }

    public String nomStrategie()
    {
        return "Stratégie: Priorité Rédacteurs.";
    }
}
