package linda.applications.eratosthene;


import java.util.Date;

import linda.Linda;
import linda.Tuple;

public class EratostheneSequentiel{
	
	protected static int i, limit;
	
	public static void main(String[] args) {
		
		Linda linda = new linda.shm.CentralizedLinda();
		int max;
		
		try{
			max = Integer.parseInt(args[0]);
			
			for (i = 2; i<=max; i++) {
				linda.write(new Tuple(i));
			}
			
			System.out.println("Tableau d'entiers construit");
			long time = (new Date()).getTime();
			limit = (int) Math.floor(Math.sqrt(max));
			int i=2;
			while (i<=limit){
				if (linda.tryRead(new Tuple(i))!=null)
					tryTakeMultiples(linda, i, max);
				i++;
			}
			time = new Date().getTime() - time;
			System.out.println("Temps d'execution: "+time+" ms");
			System.out.println(linda.readAll(new Tuple(Integer.class)).toString().length());
		}
		catch (NumberFormatException e) {
			System.out.println("Erreur, ce n'est pas un entier.");
		}
		
	}
	
	private static void tryTakeMultiples(Linda linda, int i, int max) {
		int j=2;
		while (j*i<=max) {
			linda.tryTake(new Tuple(j*i));
			j++;
		}
	}
}
