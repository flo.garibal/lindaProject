package linda.outils;

import java.util.ArrayList;
import java.util.List;

public class BarriereLinda{

	List<SemaphoreLinda> barriere;
	
	public BarriereLinda(int nb){
		barriere=new ArrayList<SemaphoreLinda>();
		for (int i=0;i<nb;i++){
			barriere.add(new SemaphoreLinda("semaphore", 1));
		}
	}
	
	public void await() {
		for (SemaphoreLinda s:barriere){
			s.acquire();
		}
	}
	
	public void release(int i){
		barriere.get(i).release();
	}
	
	public void acquire(int i){
		barriere.get(i).acquire();
	}
}
