package linda.outils;

import java.util.Collection;
import java.util.Scanner;

import linda.AsynchronousCallback;
import linda.Callback;
import linda.Linda;
import linda.Tuple;
import linda.Linda.eventMode;
import linda.Linda.eventTiming;
/**
 * @author ndupuis
 *
 */
public class InterpreteurServer {
	
	static String version="0.8";
	static int sleep;
	static Tuple res;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		/* Implémentation de l'interpreteur de commande Linda */
		Linda linda;
		try {
			linda = (Linda) new linda.server.LindaClient(args[0]);
		}
		catch (Exception e){
			e.printStackTrace();
			return;
		}
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Bienvenue dans l'interpréteur Linda");
		System.out.println("-----------------------------------");
		System.out.println("Version "+version);
		
		boolean continuer = true;
		String[] cmdsplit;
		while (continuer) {
			
			/* On demande d'entrer une commande */
			sleep = 0;
			System.out.print("Votre commande > ");
			cmdsplit=sc.nextLine().split(" ");
			
			/* On sélectionne selon le premier mot de la commande */
			switch (cmdsplit[0].toLowerCase()) {
			/* quitter, exit, bye permette de quitter l'interpreteur */
			case "quitter": case "exit": case "bye": 
				continuer = false;
				System.out.println("Sortie de l'interpréteur de commande");
				break;
			
			case "help": printhelp(); break; 
			
			case "debug": linda.debug("== "); break;
			/* Commande write */
			case "write":
				/* On récupère un Tuple à ecrire */
				res = getTuple(sc);
				
				/* On regarde si un argument de temps a été inséré */
				try {
					sleep = Integer.parseInt(cmdsplit[1]);
				}
				catch (Exception e) {
					System.out.println("Pas de sleep-time détecté. Valeur par défaut: 0");
				}
				
				/* On excécute la commande par un autre thread */
				new Thread() {
					public void run() {
						try {
		                    Thread.sleep(sleep);
		                } catch (InterruptedException e) {
		                    e.printStackTrace();
		                }
						linda.write(res);
					}
				}.start();
				break;
				
			/* Commande take */
			case "take":
				/* On récupère un motif */
				res = getMotif(sc);
				
				try {
					sleep = Integer.parseInt(cmdsplit[1]);
				}
				catch (Exception e) {
					System.out.println("Pas de sleep-time détecté. Valeur par défaut: 0");
				}
				
				new Thread() {
					public void run() {
						try {
		                    Thread.sleep(sleep);
		                } catch (InterruptedException e) {
		                    e.printStackTrace();
		                }
						System.out.println(linda.take(res));
					}
				}.start();
				break;
				
			/*Commande read */
			case "read":
				
				res = getMotif(sc);
				
				try {
					sleep = Integer.parseInt(cmdsplit[1]);
				}
				catch (Exception e) {
					System.out.println("Pas de sleep-time détecté. Valeur par défaut: 0");
				}
				
				new Thread() {
					public void run() {
						try {
		                    Thread.sleep(sleep);
		                } catch (InterruptedException e) {
		                    e.printStackTrace();
		                }
						System.out.println(linda.read(res));
					}
				}.start();
				break;
				
			/* Commande tryTake */
			case "trytake":
				
				res = getMotif(sc);
				
				try {
					sleep = Integer.parseInt(cmdsplit[1]);
				}
				catch (Exception e) {
					System.out.println("Pas de sleep-time détecté. Valeur par défaut: 0");
				}
				
				new Thread() {
					public void run() {
						try {
		                    Thread.sleep(sleep);
		                } catch (InterruptedException e) {
		                    e.printStackTrace();
		                }
						System.out.println(linda.tryTake(res));
					}
				}.start();
				break;
			
			/* Commande tryRead */
			case "tryread":
				
				res = getMotif(sc);
				
				try {
					sleep = Integer.parseInt(cmdsplit[1]);
				}
				catch (Exception e) {
					System.out.println("Pas de sleep-time détecté. Valeur par défaut: 0");
				}
				
				new Thread() {
					public void run() {
						try {
		                    Thread.sleep(sleep);
		                } catch (InterruptedException e) {
		                    e.printStackTrace();
		                }
						System.out.println(linda.tryRead(res));
					}
				}.start();
				break;
				
			/* Commande takeAll */
			case "takeall":
				
				res = getMotif(sc);
				
				try {
					sleep = Integer.parseInt(cmdsplit[1]);
				}
				catch (Exception e) {
					System.out.println("Pas de sleep-time détecté. Valeur par défaut: 0");
				}
				
				new Thread() {
					public void run() {
						try {
		                    Thread.sleep(sleep);
		                } catch (InterruptedException e) {
		                    e.printStackTrace();
		                }
						collectionPrint(linda.takeAll(res));
					}
				}.start();
				break;
				
			/* Commande readAll */	
			case "readall":
				
				res = getMotif(sc);
				
				try {
					sleep = Integer.parseInt(cmdsplit[1]);
				}
				catch (Exception e) {
					System.out.println("Pas de sleep-time détecté. Valeur par défaut: 0");
				}
				
				new Thread() {
					public void run() {
						try {
		                    Thread.sleep(sleep);
		                } catch (InterruptedException e) {
		                    e.printStackTrace();
		                }
						collectionPrint(linda.readAll(res));
					}
				}.start();
				break;
				
			/* Commande eventRegister */
			case "eventregister":
				
				/* Récupérer mode read ou take */
				System.out.print("Entrez le mode (read ou take): ");
				String line = sc.nextLine();
				Linda.eventMode ev;;
				if (line.equalsIgnoreCase("read")) {
					ev = eventMode.READ;
				}
				else if (line.equalsIgnoreCase("take")){
					ev = eventMode.TAKE;
				}
				else {
					System.out.println("Mode inconnu. Recommencez.");
					break;
				}
				
				/* Récupérer timing immediate ou future */
				System.out.print("Entrez le timing (immediate ou future): ");
				line = sc.nextLine();
				Linda.eventTiming evt;
				if (line.equalsIgnoreCase("immediate")) {
					evt = eventTiming.IMMEDIATE;
				}
				else if (line.equalsIgnoreCase("future")){
					evt = eventTiming.FUTURE;
				}
				else {
					System.out.println("Timing inconnu. Recommencez.");
					break;
				}
				
				/* Récuperer motif */
				System.out.println("Entrez le motif.");
				res = getMotif(sc);
				
				/* Récupérer une class de callback */
				System.out.print("Choisissez un callback: ");
				line = sc.nextLine();
				boolean isCallback = false;
				Class<?> callback = null;
				try {
					callback = Class.forName(line);
				} catch (ClassNotFoundException e2) {
					System.out.println("Classe inconnue.");
					break;
				}
				
				/* Verifier que c'est bien un Callback */
				Class<?>[] interfaces = null;
				interfaces = callback.getInterfaces();
				for (int i = 0; i<interfaces.length;i++) {
					if (interfaces[i].getName().equals("linda.Callback"))
						isCallback = true;
				}
				if (!isCallback) {
					System.out.println("Ce n'est pas une classe implémentant un callback");
					break;
				}
				else{
					
					/* instancier ce callback */
					try {
						Callback cb = (Callback) callback.newInstance();
						
						/* Demander s'il doit être synchrone, ou asynchrone */
						System.out.print("Callback synchrone? (o/n) ");
						line = sc.nextLine();
						if (line.equalsIgnoreCase("n") || line.equalsIgnoreCase("non"))
							linda.eventRegister(ev, evt, res, cb);
						else if (line.equalsIgnoreCase("o") || line.equalsIgnoreCase("Oui"))
							linda.eventRegister(ev, evt, res, new AsynchronousCallback(cb));
						else {
							System.out.println("Réponse inapropriée");
							break;
						}
					} catch (InstantiationException | IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}				
				break;
			default: System.out.println("Commande inconnue (write, take, read, takeAll, readAll, tryTake, tryRead, eventregister).");
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
	private static void printhelp() {
		// TODO Auto-generated method stub
		System.out.println("Aide de l'interpréteur:");
		System.out.println("-----------------------\n");
		System.out.println("Commandes disponibles: \n- write \n- read\n- take\n- readall \n- takeall\n- tryread\n- trytake\n- eventregister\n");
		
		System.out.println("Saisie d'un tuple:\n");
		System.out.println("L'interpréteur va demander le type de donnée, et la valeur pour chaque élément du tuple.\n Les types disponibles sont: Tuple, Boolean, Integer, String.");
		System.out.println("Pour terminer la saisie, entre \"q\" comme type.\n");
		
		System.out.println("Saisie d'un motif:\n");
		System.out.println("L'interpréteur va demander s'il s'agit d'une classe, ou d'un objet, et la classe (et sa valeur si objet).\n Les classes disponibles sont: Object, Tuple, Boolean, Integer, String.");
		System.out.println("Pour terminer la saisie, entre \"q\" comme type.\n");
		
		System.out.println("Saisie d'un eventregister:\n");
		System.out.println("- Entrer eventregister dans l'invite de commande.");
		System.out.println("- Entrer le mode 'read' ou 'take'.");
		System.out.println("- Enrer le timing 'immediate' ou 'future'.");
		System.out.println("- Entrer le motif.");
		System.out.println("- Entrer la classe qui implémente le Callback.");
		System.out.println("- Entrer si le callback doit être synchrone ou non (o/n).");
		System.out.println("- Tester.");
	}
	protected static void collectionPrint(Collection<Tuple> takeAll) {
		// TODO Auto-generated method stub
		System.out.print("{ ");
		for (Tuple t: takeAll) {
			System.out.print(t+" ");
		}
		System.out.println(" }");
		
	}
	private static Tuple getTuple(Scanner sc) {
		System.out.println("Vous allez créer un tuple.");
		Tuple res = new Tuple();
		String line = "";
		while (!line.equals("q")){
			System.out.print("Entrez le type: ");
			line = sc.nextLine();
			switch (line.toLowerCase()) {
				case "q": System.out.println("Tuple créé: "+res);break;
				case "integer":
					try{
						System.out.print("Entrez la valeur: ");
						res.add(Integer.parseInt(sc.nextLine()));
					}
					catch (Exception e){
						System.out.println("Ce n'est pas un entier! Valeur annulée, réessayez.");
					}
					break;
				case "string":
					System.out.print("Entrez la valeur: ");
					res.add(sc.nextLine());
					break;
				case "boolean":
					try{
						System.out.print("Entrez la valeur: ");
						line = sc.nextLine();
						switch (line.toLowerCase()) {
						case "true":
							res.add(true);
							break;
						default: res.add(false);
						}
					}
					catch (Exception e){
						System.out.println("Ce n'est pas un booléen! Valeur annulée, réessayez.");
					}
					break;
				case "tuple":
					res.add(getTuple(sc));
					break;
				default: System.out.println("Type inconnu, réesayez.");
			}
		}
		return res;
	}
	
	private static Tuple getMotif(Scanner sc) {
		System.out.println("Vous allez créer un tuple de motif.");
		Tuple res = new Tuple();
		String line = "";
		while (!line.equalsIgnoreCase("q")){
			System.out.print("Classe ou objet? ");
			line = sc.nextLine();
			switch (line.toLowerCase()) {
			case "q": System.out.println("Motif obtenu: "+res);break;
			case "classe":
				System.out.print("Entrez le type: ");
				line = sc.nextLine();
				switch (line.toLowerCase()) {
					case "integer": res.add(Integer.class);break;
					case "string": res.add(String.class); break;
					case "boolean": res.add(Boolean.class); break;
					case "tuple": res.add(Tuple.class); break;
					case "object": res.add(Object.class); break;
					default: System.out.println("Type inconnu, réesayez.");
				}
				break;
			case "objet":
				System.out.print("Entrez le type: ");
				line = sc.nextLine();
				switch (line.toLowerCase()) {
					case "integer":
						try{
							System.out.print("Entrez la valeur: ");
							res.add(Integer.parseInt(sc.nextLine()));
						}
						catch (Exception e){
							System.out.println("Ce n'est pas un entier! Valeur annulée, réessayez.");
						}
						break;
					case "string":
						System.out.print("Entrez la valeur: ");
						res.add(sc.nextLine());
						break;
					case "boolean":
						try{
							System.out.print("Entrez la valeur: ");
							res.add(Boolean.getBoolean(sc.nextLine()));
						}
						catch (Exception e){
							System.out.println("Ce n'est pas un booléen! Valeur annulée, réessayez.");
						}
						break;
					case "tuple":
						res.add(getMotif(sc));
						break;
					default: System.out.println("Type inconnu, réesayez.");
				}
			default: System.out.println("Veillez entrer Objet ou Classe");
			}
		}
		return res;
	}

}
