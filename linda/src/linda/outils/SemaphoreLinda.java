package linda.outils;

import linda.Linda;
import linda.Tuple;

public class SemaphoreLinda {
	private linda.Linda linda;
	private String semaphoreId;
	private int number;
	
	public SemaphoreLinda(String sId, int nb) {
		linda = new linda.shm.CentralizedLinda();
		semaphoreId = sId;
		number = nb;
		
		init();
	}
	
	private void init() {
		Tuple t = new Tuple(semaphoreId);
		for (int i = 0; i < number; i++) {
			linda.write(t);
		}
	}
	
	public void acquire() {
		linda.take(new Tuple(semaphoreId));
	}
	
	public void release() {
		linda.write(new Tuple(semaphoreId));
	}
}
