/**
 * 
 */
package linda.outils;

import linda.Callback;
import linda.Tuple;

/**
 * @author noel
 *
 */
public class MyCallback implements Callback {

	/**
	 * 
	 */
	public MyCallback() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see linda.Callback#call(linda.Tuple)
	 */
	@Override
	public void call(Tuple t) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        System.out.println("Got "+t);
    }

}
