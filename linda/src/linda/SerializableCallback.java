package linda;

import java.io.Serializable;
import java.rmi.RemoteException;

public class SerializableCallback implements Serializable {
	
	private Callback cb;

    public SerializableCallback (Callback cb) { this.cb = cb; }
        
    /** Asynchronous call: the associated callback is concurrently run and this one immediately returns.
     * */
    public void call(final Tuple t) {
        new Thread() {
            public void run() {
                cb.call(t);
            }
        }.start();
    }
}
